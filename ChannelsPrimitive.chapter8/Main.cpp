#include <systemc.h>
#include <iostream>
#include "BasicChannelExample.h"

using namespace std;

int sc_main( int, char *[])
{
    sc_event isr_event;
    CBusFifo cbus_fifo;
    sc_signal<sc_time> snapshot;

    new bpe( __PRETTY_FUNCTION__ , sc_time(1,SC_MS), & isr_event, & cbus_fifo, & snapshot );
    new isr( ".notifier", & isr_event, & cbus_fifo, & snapshot );

    sc_start(40, SC_MS );
    cout << sc_time_stamp() <<  " done" << endl;
    return 0;
}
