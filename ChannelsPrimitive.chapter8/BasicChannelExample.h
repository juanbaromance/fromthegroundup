#ifndef BPEinc
#define BPEinc

#include <systemc.h>

#include <vector>
namespace CBusFifoNameSpace
{

typedef int payloadT;
typedef sc_fifo<payloadT> CBusFifo;
typedef sc_signal<sc_time> SnapShot;
};


using namespace CBusFifoNameSpace;
SC_MODULE(bpe)
{
    typedef bpe SC_CURRENT_USER_MODULE;
    bpe( sc_module_name name, sc_time slice_probe, sc_event *isr_event, CBusFifo *fifo, SnapShot *s ) :
     sc_module( name ), slice( slice_probe ), event( isr_event ), cbus_fifo( fifo ), snapshot(s)
    {
        SC_THREAD(entry);
    }
    
    sc_time slice;
    sc_event *event;
    CBusFifo *cbus_fifo;
    SnapShot *snapshot;
    void entry();
};


SC_MODULE(isr)
{
    typedef isr SC_CURRENT_USER_MODULE;
    isr( sc_module_name name, sc_event *isr_event, CBusFifo *fifo, SnapShot *s ) : 
        sc_module( name ), event( isr_event ), cbus_fifo(fifo), snapshot(s) 
    {
        SC_THREAD(Periodic);
        SC_THREAD(BurstHandler);
    }
    
    SnapShot *snapshot;
    sc_mutex mtx;
    sc_event *event;
    CBusFifo *cbus_fifo;
    void Periodic();
    void BurstHandler();
};



#endif
