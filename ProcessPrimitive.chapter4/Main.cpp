#include <systemc.h>
#include <iostream>
#include "BasicProcessExample.h"

using namespace std;

int sc_main( int, char *[])
{
    sc_event isr_event;
    CBusFifo cbus_fifo;

    new bpe( __PRETTY_FUNCTION__ , sc_time(1,SC_MS), & isr_event, & cbus_fifo );
    new isr( ".notifier", & isr_event, & cbus_fifo );

    sc_start(40, SC_MS );
    cout << sc_time_stamp() <<  " done" << endl;
    return 0;
}
