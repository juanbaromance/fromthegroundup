#ifndef BPEinc
#define BPEinc

#include <systemc.h>

#include <vector>
namespace CBusFifoNameSpace
{

typedef int payloadT;
typedef sc_fifo<payloadT> CBusFifo;

};


using namespace CBusFifoNameSpace;
SC_MODULE(bpe)
{
    typedef bpe SC_CURRENT_USER_MODULE;
    bpe( sc_module_name name, sc_time slice_probe, sc_event *isr_event, CBusFifo *fifo ) : sc_module( name ),
        slice( slice_probe ), event( isr_event ), cbus_fifo( fifo )
    {
        SC_THREAD(entry);
    }
    sc_time slice;
    sc_event *event;
    CBusFifo *cbus_fifo;
    void entry();
};


SC_MODULE(isr)
{
    typedef isr SC_CURRENT_USER_MODULE;
    isr( sc_module_name name, sc_event *isr_event, CBusFifo *fifo ) : sc_module( name ), event( isr_event ), cbus_fifo(fifo)
    {
        SC_THREAD(Periodic);
        SC_THREAD(BurstHandler);
    }

    sc_event *event;
    CBusFifo *cbus_fifo;
    void Periodic();
    void BurstHandler();
};



#endif
