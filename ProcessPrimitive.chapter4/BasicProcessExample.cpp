#include "BasicProcessExample.h"
#include <iostream>
#include <string>
using namespace std;

void bpe::entry()
{
    string context = __PRETTY_FUNCTION__ ;
    sc_time t = sc_time_stamp();
    payloadT payload = 0;

    while( 1 )
    {
        sc_time current = sc_time_stamp();
        wait( this->slice );
        if( ( current - t ) >= sc_time(10,SC_MS) )
        {
            /* Now More business rules could select the precise notification way */
            event->notify(2,SC_MS);
            event->notify(SC_ZERO_TIME);
            event->notify();
            event->cancel();
            wait( sc_time(5,SC_MS), *event );
            cout << context << ( event->triggered()  ? ": synced @ "  : ": handshake l o s t : consumer stallen @ " ) << sc_time_stamp() << endl;
            t = event->triggered() ? current : t;
            continue;
        }
        cbus_fifo->write( payload++ );

    }
}

void isr::Periodic()
{
    string context = __PRETTY_FUNCTION__ ;
    cout << context << ": cycled" << endl;

    while( 1  )
    {
        wait( sc_time(10,SC_MS), *event );
        if( event->triggered() )
            cout << context << " @ " << sc_time_stamp() <<  endl;
    }

}

#include <bitset>
void isr::BurstHandler()
{
    while( 1 )
    {
        wait( *event );
        string context = __PRETTY_FUNCTION__ ;
        bool glitch = ( cbus_fifo->num_available() == 0 );
        cout << context << " " << ( glitch ? "glitched": "triggered" ) << " @ " << sc_time_stamp();
        if( glitch == false )
        {
            payloadT payload;
            cout << "[ ";
            while( cbus_fifo->nb_read( payload ) )
                printf("%02x", payload );
            cout << " ] : " << "(done)\n";
            event->notify();
        }
        cout << endl;
    }
}
